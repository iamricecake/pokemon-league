/*
 * Color scheme inspired by https://www.schemecolor.com/umbreon-pokemon-color-scheme.php
 */

const theme = {
  white: '#FFFFFF',
  whiteAsh: '#DEF1FF',
  coldAsh: '#CDDFEC',
  darkPichu: '#BEAA24',
  saffron: '#E6CE2B',
  lightMisty: '#EED62D',
  misty: '#E9D12C',
  shandy: '#FFF06A',
  lightCharcoal: '#51585D',
  darkCharcoal: '#2F3336',
  darkrai: '#222528',
  mauvelous: '#F099A8',
  espeon: '#8B647D',
  darkEspeon: '#715266',
  umbreon: '#2D2028',
};

export default theme;
