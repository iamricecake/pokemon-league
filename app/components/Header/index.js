import React from 'react';
import styled from 'styled-components';

import PokeBall from 'images/pokeball.svg';

const Logo = styled.img`
  display: inline-block;
  height: 60px;
  margin-right: 10px;
  width: 60px;
`;

const Title = styled.h1`
  align-items: center;
  display: flex;
  justify-content: center;
  margin: 0;
  padding: 0;
`;

export default function Header() {
  return (
    <div>
      <Title>
        <Logo src={PokeBall} alt="PokeBall" />
        <span>Pokemon League Lineup Builder</span>
      </Title>
    </div>
  );
}
