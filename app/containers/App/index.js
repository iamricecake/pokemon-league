import React from 'react';
import { Helmet } from 'react-helmet';
import styled, { ThemeProvider } from 'styled-components';
import { Flex, Box } from '@rebass/grid';
import { Switch, Route } from 'react-router-dom';

import theme from 'theme';
import Header from 'components/Header';
import HomePage from 'containers/HomePage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

const AppContainer = styled.div`
  background: ${props => props.theme.shandy};
  color: ${props => props.theme.darkCharcoal};
`;

const AppWrapper = styled(Flex).attrs({
  flexDirection: 'column',
  mx: 'auto',
  pb: 3,
})`
  min-height: 100vh;
  max-width: 1280px;
`;

const Content = styled(Flex)`
  align-items: stretch;
  flex: 1 0 auto;
`;

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <AppContainer>
        <Helmet
          titleTemplate="%s - Pokemon League Lineup Builder"
          defaultTitle="Pokemon League Lineup Builder"
        >
          <meta
            name="description"
            content="Build your Pokemon lineup and become the very best!"
          />
        </Helmet>
        <AppWrapper>
          <Box pt={3} pb={4}>
            <Header />
          </Box>
          <Content>
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route component={NotFoundPage} />
            </Switch>
          </Content>
        </AppWrapper>
      </AppContainer>
    </ThemeProvider>
  );
}
