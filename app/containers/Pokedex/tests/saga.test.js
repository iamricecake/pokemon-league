import { call, select, put, takeLatest } from 'redux-saga/effects';

import { SEARCH_POKEMON } from 'containers/Pokedex/constants';
import {
  searchService,
  pokemonFound,
  pokemonFoundFromCache,
  pokemonNotFound,
} from 'containers/Pokedex/actions';
import { findPokemonFromQuery } from 'containers/Pokedex/selectors';
import Api from 'Api';

import pokedexData, { findPokemon } from '../saga';

/* eslint-disable redux-saga/yield-effects */
describe('findPokemon Saga', () => {
  let findPokemonGenerator;

  const query = 'pichu';

  describe('found pokemon from cache', () => {
    const pokemon = { foo: { bar: 1 } };

    beforeEach(() => {
      findPokemonGenerator = findPokemon({ payload: { query } });

      const selectDescriptor = findPokemonGenerator.next().value;
      expect(selectDescriptor).toEqual(select(findPokemonFromQuery));
    });

    it('dispatches the pokemonFoundFromCache action', () => {
      const putDescriptor = findPokemonGenerator.next(pokemon).value;
      expect(putDescriptor).toEqual(put(pokemonFoundFromCache(pokemon)));
    });
  });

  describe('did not find pokemon from cache', () => {
    beforeEach(() => {
      findPokemonGenerator = findPokemon({ payload: { query } });

      const selectDescriptor = findPokemonGenerator.next().value;
      expect(selectDescriptor).toEqual(select(findPokemonFromQuery));

      const putDescriptor = findPokemonGenerator.next(undefined).value;
      expect(putDescriptor).toEqual(put(searchService()));

      const callDescriptor = findPokemonGenerator.next().value;
      expect(callDescriptor).toEqual(call(Api.findPokemon, query));
    });

    it('dispatches the pokemonFound action if it requests the data successfully', () => {
      const response = { foo: 1 };
      const putDescriptor = findPokemonGenerator.next(response).value;
      expect(putDescriptor).toEqual(put(pokemonFound(response)));
    });

    it('calls the pokemonNotFound action if the response errors', () => {
      const response = new Error('Some error');
      const putDescriptor = findPokemonGenerator.throw(response).value;
      expect(putDescriptor).toEqual(put(pokemonNotFound(response)));
    });
  });
});

describe('pokedexData Saga', () => {
  const pokedexDataSaga = pokedexData();

  it('should start task to watch for SEARCH_POKEMON action', () => {
    const takeLatestDescriptor = pokedexDataSaga.next().value;
    expect(takeLatestDescriptor).toEqual(
      takeLatest(SEARCH_POKEMON, findPokemon),
    );
  });
});
