import { fromJS } from 'immutable';
import pokedexReducer, { initialState } from '../reducer';
import { searchService, pokemonFound, pokemonNotFound } from '../actions';

describe('pokedexReducer', () => {
  let state;

  beforeEach(() => {
    state = fromJS({});
  });

  it('returns the initial state', () => {
    expect(pokedexReducer(undefined, {})).toEqual(initialState);
  });

  it('handles SEARCH_SERVICE', () => {
    expect(pokedexReducer(state, searchService('pichu'))).toMatchSnapshot();
  });

  it('handles POKEMON_FOUND', () => {
    const pokemon = { foo: 1 };
    expect(pokedexReducer(state, pokemonFound(pokemon))).toMatchSnapshot();
  });

  it('handles POKEMON_NOT_FOUND', () => {
    const error = new Error('Failed');
    expect(pokedexReducer(state, pokemonNotFound(error))).toMatchSnapshot();
  });
});
