import React from 'react';
import { shallow } from 'enzyme';

import { Pokedex, mapDispatchToProps } from '../index';
import SearchForm from '../SearchForm';
import PokemonData from '../PokemonData';
import { searchPokemon, addToLineup } from '../actions';

describe('<Pokedex />', () => {
  describe('Component', () => {
    let renderedComponent;

    const onSearch = jest.fn();
    const onAddClick = jest.fn();
    const pokemon = { foo: 1 };

    beforeEach(() => {
      renderedComponent = shallow(
        <Pokedex
          pokemon={pokemon}
          notFound
          isSearching
          isAddDisabled
          onAddClick={onAddClick}
          onSearch={onSearch}
        />,
      );
    });

    it('renders the search form', () => {
      expect(
        renderedComponent.contains(<SearchForm onSearch={onSearch} />),
      ).toEqual(true);
    });

    it('renders the pokemon data', () => {
      expect(
        renderedComponent.contains(
          <PokemonData
            pokemon={pokemon}
            notFound
            isSearching
            isAddDisabled
            onAddClick={onAddClick}
          />,
        ),
      ).toEqual(true);
    });
  });

  describe('mapDispatchToProps', () => {
    const dispatchSpy = jest.fn();
    const props = mapDispatchToProps(dispatchSpy);

    describe('onSearch', () => {
      it('dispatches the searchPokemon action', () => {
        const query = 'greninja';
        props.onSearch(query);
        expect(dispatchSpy).toHaveBeenCalledWith(searchPokemon(query));
      });
    });

    describe('onAddClick', () => {
      it('dispatches the addToLineup action', () => {
        const name = 'greninja';
        props.onAddClick(name);
        expect(dispatchSpy).toHaveBeenCalledWith(addToLineup(name));
      });
    });
  });
});
