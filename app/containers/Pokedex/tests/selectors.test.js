import { fromJS } from 'immutable';

import {
  getPokedex,
  selectIsSearching,
  selectNotFound,
  getQuery,
  getPokemonByName,
  getActivePokemonName,
  findPokemonFromQuery,
  activePokemon,
} from '../selectors';

describe('getPokedex', () => {
  it('selects the pokedex state', () => {
    const pokedexState = fromJS({});
    const mockedState = fromJS({
      pokedex: pokedexState,
    });
    expect(getPokedex(mockedState)).toEqual(pokedexState);
  });
});

describe('selectIsSearching', () => {
  it('selects isSearching', () => {
    const mockedState = fromJS({
      pokedex: {
        isSearching: true,
      },
    });
    expect(selectIsSearching(mockedState)).toEqual(true);
  });
});

describe('selectNotFound', () => {
  it('selects notFound', () => {
    const mockedState = fromJS({
      pokedex: {
        notFound: true,
      },
    });
    expect(selectNotFound(mockedState)).toEqual(true);
  });
});

describe('getQuery', () => {
  it('selects query', () => {
    const mockedState = fromJS({
      pokedex: {
        query: 'foo',
      },
    });
    expect(getQuery(mockedState)).toEqual('foo');
  });
});

describe('getActivePokemonName', () => {
  it('selects activePokemonName', () => {
    const mockedState = fromJS({
      pokedex: {
        activePokemonName: 'foo',
      },
    });
    expect(getActivePokemonName(mockedState)).toEqual('foo');
  });
});

describe('getPokemonByName', () => {
  it('selects notFound', () => {
    const byNameState = fromJS({
      foo: { bar: 1 },
    });
    const mockedState = fromJS({
      pokedex: {
        pokemon: {
          byName: byNameState,
        },
      },
    });
    expect(getPokemonByName(mockedState)).toEqual(byNameState);
  });
});

describe('findPokemonFromQuery', () => {
  it('selects matching pokemon', () => {
    const pichuState = fromJS({
      foo: { bar: 1 },
    });
    const mockedState = fromJS({
      pokedex: {
        query: 'Pichu',
        pokemon: {
          byName: {
            pichu: pichuState,
          },
        },
      },
    });
    expect(findPokemonFromQuery(mockedState)).toEqual(pichuState.toJS());
  });
});

describe('activePokemon', () => {
  it('selects matching pokemon', () => {
    const pichuState = fromJS({
      foo: { bar: 1 },
    });
    const mockedState = fromJS({
      pokedex: {
        activePokemonName: 'pichu',
        pokemon: {
          byName: {
            pichu: pichuState,
          },
        },
      },
    });
    expect(activePokemon(mockedState)).toEqual(pichuState.toJS());
  });
});
