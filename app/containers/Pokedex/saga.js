import { call, select, put, takeLatest } from 'redux-saga/effects';
import { SEARCH_POKEMON } from 'containers/Pokedex/constants';
import Api from 'Api';
import {
  searchService,
  pokemonFound,
  pokemonFoundFromCache,
  pokemonNotFound,
} from 'containers/Pokedex/actions';
import { findPokemonFromQuery } from 'containers/Pokedex/selectors';

export function* findPokemon(action) {
  const { query } = action.payload;

  try {
    const cachedPokemon = yield select(findPokemonFromQuery);

    if (cachedPokemon) {
      yield put(pokemonFoundFromCache(cachedPokemon));
    } else {
      yield put(searchService());
      const pokemon = yield call(Api.findPokemon, query);
      yield put(pokemonFound(pokemon));
    }
  } catch (err) {
    // For now, to simplify things, we'll just treat
    // all errors as not found.
    yield put(pokemonNotFound(err));
  }
}

export default function* pokedexData() {
  yield takeLatest(SEARCH_POKEMON, findPokemon);
}
