import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex, Box } from '@rebass/grid';
import Img from 'react-image';

import Stats from './Stats';

const Wrapper = styled.div`
  font-size: 18px;
`;

const ImageWrapper = styled.div`
  background: ${props => props.theme.whiteAsh};
  border-radius: 8px;
  height: 96px;
  width: 96px;
`;

const Name = styled.span`
  display: block;
  font-size: 32px;
  font-weight: bold;
  margin-bottom: 4px;
  text-transform: capitalize;
`;

const AddButton = styled.button`
  background: ${props =>
    props.isAddDisabled ? props.theme.lightCharcoal : props.theme.mauvelous};
  border-radius: 8px;
  color: ${props =>
    props.isAddDisabled ? props.theme.coldAsh : props.theme.white};
  cursor: pointer;
  display: block;
  font-size: 24px;
  outline: none;
  padding: 8px 16px;
`;

const Label = styled.span`
  display: block;
  font-size: 16px;
  font-weight: bold;
`;

const Text = styled.span`
  display: block;
  font-size: 16px;
`;

const FieldGroup = styled(Box).attrs({
  px: 2,
  py: 3,
})`
  border-top: 1px solid ${props => props.theme.darkCharcoal};

  &:last-child {
    padding-bottom: 0;
  }
`;

class Pokemon extends React.PureComponent {
  constructor(props) {
    super(props);

    this.handleAddClick = this.handleAddClick.bind(this);
  }

  handleAddClick() {
    if (!this.props.isAddDisabled) {
      this.props.onAddClick(this.props.name);
    }
  }

  render() {
    const { name, types, image, abilities, stats, isAddDisabled } = this.props;

    return (
      <Wrapper>
        <Flex pb={3}>
          <Box pr={3}>
            <ImageWrapper>
              <Img src={image} />
            </ImageWrapper>
          </Box>
          <Box flex="1 0 auto">
            <Name>{name}</Name>
            <AddButton
              isAddDisabled={isAddDisabled}
              onClick={this.handleAddClick}
            >
              Add To Lineup
            </AddButton>
          </Box>
        </Flex>
        <div>
          <FieldGroup>
            <Label>Type(s)</Label>
            <Text>{types.join(', ')}</Text>
          </FieldGroup>
          <FieldGroup>
            <Label>Abilities</Label>
            <Text>{abilities.join(', ')}</Text>
          </FieldGroup>
          <FieldGroup>
            <Label>Stats</Label>
            <Stats stats={stats} />
          </FieldGroup>
        </div>
      </Wrapper>
    );
  }
}

Pokemon.propTypes = {
  name: PropTypes.string.isRequired,
  types: PropTypes.array.isRequired,
  image: PropTypes.string.isRequired,
  abilities: PropTypes.array.isRequired,
  stats: PropTypes.array.isRequired,
  onAddClick: PropTypes.func.isRequired,
  isAddDisabled: PropTypes.bool.isRequired,
};

export default Pokemon;
