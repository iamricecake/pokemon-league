import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex, Box } from '@rebass/grid';

const Wrapper = styled(Flex)`
  flex-wrap: wrap;
`;

const Label = styled.span`
  color: ${props => props.theme.darkrai};
  display: block;
`;

const Value = styled.span`
  color: ${props => props.theme.lightCharcoal};
  display: block;
`;

function Stats({ stats }) {
  return (
    <Wrapper>
      {stats.map(s => (
        <Box key={s.name} width={1 / 2}>
          <Label>{s.name}</Label>
          <Value>{s.value}</Value>
        </Box>
      ))}
    </Wrapper>
  );
}

Stats.propTypes = {
  stats: PropTypes.array.isRequired,
};

export default Stats;
