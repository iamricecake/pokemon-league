import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Flex, Box } from '@rebass/grid';

const Input = styled.input`
  background: ${props => props.theme.darkCharcoal};
  border: 4px solid ${props => props.theme.darkrai};
  border-radius: 8px;
  color: ${props => props.theme.coldAsh};
  font-size: 24px;
  outline: none;
  padding: 8px;
  width: 100%;
`;

const Submit = styled.input.attrs({
  type: 'submit',
})`
  background: ${props => props.theme.mauvelous};
  border: none;
  border-radius: 8px;
  color: ${props => props.theme.white};
  cursor: pointer;
  font-size: 24px;
  outline: none;
  padding: 12px;
`;

/* eslint-disable react/prefer-stateless-function */
class SearchForm extends React.PureComponent {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    const query = event.target.query.value.trim();

    event.preventDefault();

    if (query !== '') {
      this.props.onSearch(query);
    }
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <Flex>
          <Box pr={3} flex="1 1 auto">
            <Input name="query" placeholder="Try &quot;Greninja&quot;" />
          </Box>
          <Box>
            <Submit value="Go" />
          </Box>
        </Flex>
      </form>
    );
  }
}

SearchForm.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

export default SearchForm;
