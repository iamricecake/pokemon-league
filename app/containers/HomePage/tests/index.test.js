import React from 'react';
import { shallow } from 'enzyme';

import Pokedex from 'containers/Pokedex';
import HomePage from '../index';

describe('<HomePage />', () => {
  let renderedComponent;

  beforeEach(() => {
    renderedComponent = shallow(<HomePage />);
  });

  it('renders the Pokedex', () => {
    expect(renderedComponent.contains(<Pokedex />)).toEqual(true);
  });
});
