import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import times from 'lodash/times';
import { Flex, Box } from '@rebass/grid';
import Img from 'react-image';

import injectReducer from 'utils/injectReducer';
import { getMembers, LINEUP_LIMIT } from './selectors';
import { viewMember } from './actions';
import reducer from './reducer';

const Wrapper = styled(Flex).attrs({
  p: 1,
})`
  background: ${props => props.theme.saffron};
  border: 4px solid ${props => props.theme.darkPichu};
  border-radius: 8px;
  flex-wrap: wrap;
`;

const MemberWrapper = styled.div`
  background: ${props => props.theme.misty};
  border-radius: 8px;
  cursor: pointer;
  height: 120px;
  text-align: center;
  width: 100%;

  &:hover {
    background: ${props => props.theme.lightMisty};
  }
`;

const MemberLink = styled.a`
  display: block;
  text-decoration: none;
`;

const MemberImageWrapper = styled.div`
  display: block;
  height: 96px;
`;

const MemberName = styled.span`
  color: ${props => props.theme.darkPichu};
  display: block;
  overflow: hidden;
  position: relative;
  text-overflow: ellipsis;
  top: -4px;
  white-space: nowrap;
`;

/* eslint-disable react/prefer-stateless-function */
export class Lineup extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        {times(LINEUP_LIMIT, i => {
          const { members } = this.props;
          const data = members && members[i];
          const member = data && data.member;
          const parent = data && data.parentPokemon;
          return (
            <Box key={i} p={2} width={1 / 6} flex="1 0 auto">
              <MemberWrapper>{this.renderMember(member, parent)}</MemberWrapper>
            </Box>
          );
        })}
      </Wrapper>
    );
  }

  renderMember(member, parent) {
    if (member) {
      return (
        <MemberLink onClick={e => this.handleClick(e, member.id)}>
          <MemberImageWrapper>
            <Img src={parent.image} />
          </MemberImageWrapper>
          <MemberName>{member.name}</MemberName>
        </MemberLink>
      );
    }

    return null;
  }

  handleClick(event, index) {
    event.preventDefault();
    this.props.onMemberSelect(index);
  }
}

Lineup.propTypes = {
  members: PropTypes.array.isRequired,
  onMemberSelect: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  members: getMembers,
});

export function mapDispatchToProps(dispatch) {
  return {
    onMemberSelect: index => {
      dispatch(viewMember(index));
    },
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'lineup', reducer });

export default compose(
  withReducer,
  withConnect,
)(Lineup);
