import { VIEW_MEMBER } from './constants';

export function viewMember(id) {
  return {
    type: VIEW_MEMBER,
    payload: {
      id,
    },
  };
}
