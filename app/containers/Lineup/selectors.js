import { createSelector } from 'reselect';
import { getPokemonByName } from 'containers/Pokedex/selectors';
import { initialState } from './reducer';

export const LINEUP_LIMIT = 6;

const getLineup = state => state.get('lineup', initialState);
const getAllIds = state => getLineup(state).getIn(['pokemon', 'allIds']);
const getById = state => getLineup(state).getIn(['pokemon', 'byId']);
const getCount = state => getAllIds(state).count();
const isLineupFull = state => getCount(state) === LINEUP_LIMIT;

const buildPokemonMembers = (byId, allIds, pokemonByName) =>
  allIds
    .map(id => {
      const member = byId.get(id).toJS();
      const parentPokemon = pokemonByName.get(member.parentPokemonName).toJS();
      return { member, parentPokemon };
    })
    .toJS();

const getMembers = createSelector(
  [getById, getAllIds, getPokemonByName],
  buildPokemonMembers,
);

export { getLineup, getAllIds, getById, getCount, isLineupFull, getMembers };
