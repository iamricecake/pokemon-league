import { fromJS } from 'immutable';
import { getMembers } from '../selectors';

describe('getMembers', () => {
  it('selects the members of the lineup', () => {
    const mockedState = fromJS({
      lineup: {
        pokemon: {
          byId: {
            foo: { parentPokemonName: 'pichu' },
          },
          allIds: ['foo'],
        },
      },
      pokedex: {
        pokemon: {
          byName: {
            pichu: { name: 'pichu' },
          },
        },
      },
    });
    expect(getMembers(mockedState)).toEqual([
      {
        member: {
          parentPokemonName: 'pichu',
        },
        parentPokemon: {
          name: 'pichu',
        },
      },
    ]);
  });
});
