import { viewMember } from '../actions';
import { VIEW_MEMBER } from '../constants';

describe('Lineup actions', () => {
  describe('viewMember', () => {
    let action;

    const id = 3;

    beforeEach(() => {
      action = viewMember(id);
    });

    it('has a type of VIEW_MEMBER', () => {
      expect(action.type).toEqual(VIEW_MEMBER);
    });

    it('has the payload', () => {
      expect(action.payload).toEqual({ id });
    });
  });
});
