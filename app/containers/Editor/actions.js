import { UPDATE_NAME, REMOVE } from './constants';

export function updateName(id, value) {
  return {
    type: UPDATE_NAME,
    payload: {
      id,
      value,
    },
  };
}

export function remove(id) {
  return {
    type: REMOVE,
    payload: {
      id,
    },
  };
}
