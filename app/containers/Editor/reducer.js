import { fromJS } from 'immutable';
import { VIEW_MEMBER } from 'containers/Lineup/constants';

export const initialState = fromJS({
  activeMemberId: null,
});

function editorReducer(state = initialState, action) {
  switch (action.type) {
    case VIEW_MEMBER:
      return state.merge({
        activeMemberId: action.payload.id,
      });
    default:
      return state;
  }
}

export default editorReducer;
