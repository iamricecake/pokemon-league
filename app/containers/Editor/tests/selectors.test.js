import { fromJS } from 'immutable';
import { getMember, getParentPokemon } from '../selectors';

describe('getMember', () => {
  it('selects member', () => {
    const mockedState = fromJS({
      lineup: {
        pokemon: {
          byId: {
            abc: { name: 'foo' },
          },
        },
      },
      editor: {
        activeMemberId: 'abc',
      },
    });
    expect(getMember(mockedState)).toEqual({ name: 'foo' });
  });
});

describe('getParentPokemon', () => {
  it('selects parent pokemon', () => {
    const mockedState = fromJS({
      pokedex: {
        pokemon: {
          byName: {
            bar: { name: 'bar' },
          },
        },
      },
      lineup: {
        pokemon: {
          byId: {
            abc: { name: 'foo', parentPokemonName: 'bar' },
          },
        },
      },
      editor: {
        activeMemberId: 'abc',
      },
    });
    expect(getParentPokemon(mockedState)).toEqual({ name: 'bar' });
  });
});
