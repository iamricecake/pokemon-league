import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import styled from 'styled-components';
import { Flex, Box } from '@rebass/grid';

import injectReducer from 'utils/injectReducer';
import { getMember, getParentPokemon } from './selectors';
import { updateName, remove } from './actions';
import reducer from './reducer';

const Wrapper = styled(Box).attrs({
  p: 3,
})`
  background: ${props => props.theme.espeon};
  border: 4px solid ${props => props.theme.darkEspeon};
  border-radius: 8px;
  color: ${props => props.theme.whiteAsh};
  min-width: 100%;
`;

const ControlWrapper = styled(Box).attrs({
  pb: 3,
  mb: 3,
})`
  border-bottom: 1px solid ${props => props.theme.darkEspeon};
`;

const Input = styled.input.attrs({
  type: 'text',
})`
  background: ${props => props.theme.darkEspeon};
  border-radius: 8px;
  outline: none;
  padding: 12px 16px;
`;

const Button = styled.button`
  background: ${props => props.theme.mauvelous};
  border: 0;
  border-radius: 8px;
  cursor: pointer;
  margin-left: 8px;
  outline: none;
  padding: 12px 16px;
`;

const Details = styled(Flex)`
  align-items: stretch;
  color: ${props => props.theme.umbreon};
`;

const Label = styled.span`
  display: block;
  font-weight: bold;
`;

/* eslint-disable react/prefer-stateless-function */
export class Editor extends React.PureComponent {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
  }

  render() {
    return <Wrapper>{this.props.member && this.renderMember()}</Wrapper>;
  }

  renderMember() {
    const { types, abilities, stats } = this.props.parentPokemon;

    return (
      <div>
        <ControlWrapper>
          <Input onChange={this.handleChange} value={this.props.member.name} />
          <Button onClick={this.handleRemove}>Remove from Lineup</Button>
        </ControlWrapper>
        <Details>
          <Box width={1 / 3}>
            <Box py={2}>
              <Label>Type(s)</Label>
              <span>{types.join(', ')}</span>
            </Box>
            <Box py={2}>
              <Label>Abilities</Label>
              <span>{abilities.join(', ')}</span>
            </Box>
          </Box>
          <Box width={2 / 3}>
            <Flex alignItems="stretch" flexWrap="wrap">
              {stats.map(s => (
                <Box py={2} key={s.name} width={1 / 2}>
                  <Label>{s.name}</Label>
                  <span>{s.value}</span>
                </Box>
              ))}
            </Flex>
          </Box>
        </Details>
      </div>
    );
  }

  handleChange(event) {
    const { member, onNameChange } = this.props;
    onNameChange(member.id, event.target.value);
  }

  handleRemove() {
    const { member, onRemove } = this.props;
    onRemove(member.id);
  }
}

Editor.propTypes = {
  member: PropTypes.object,
  parentPokemon: PropTypes.object,
  onNameChange: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  member: getMember,
  parentPokemon: getParentPokemon,
});

export function mapDispatchToProps(dispatch) {
  return {
    onNameChange: (id, value) => dispatch(updateName(id, value)),
    onRemove: id => dispatch(remove(id)),
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

const withReducer = injectReducer({ key: 'editor', reducer });

export default compose(
  withReducer,
  withConnect,
)(Editor);
